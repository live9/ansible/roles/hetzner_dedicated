Hetzner Dedicated
=================

Ansible role to install the OS on Hetzner dedicated servers


Requirements
------------

* Target hosts must to be running [Hetzner's Rescue System](https://wiki.hetzner.de/index.php/Hetzner_Rescue-System/en) to get installed.
  * Strongly recomended to use ssh key: https://robot.your-server.de/key/index
  * Login in https://robot.your-server.de
  * Chose your server
  * Click in **Rescue** tab then chose Linux, 64bit, ssh key and click in Activate
  * Click in **Reset** tab then restart your server and wait 2 to 5 minutes
* The `os_image` variable must to match a name from `/root/images`.

Notes: 
* This role reads /etc/motd to validate if a host is running Hetzner's Rescue System
* If the target host is not running Hetzner's Rescue System a warning will be printed

Role Variables
--------------

The file `defaults/main.yml` contains the default values for variables you can customize for this role. Default values have the [Live9](https://live9.org) way in mind. By example the default *hostname* is the value for the ansible *inventory_hostname* variable because we use shortnames in our inventories.


Example Playbook
----------------

```
---
- name: "Provisioning Hetzner dedicated servers OS"
  hosts: hetzner-servers
  remote_user: root
  become: no
  gather_facts: no
  # variables to customize values in defaults/main.yml
  roles:
  - hetzner_dedicated

```

Author Information
------------------
Fredy Pulido
fredy at live9.org
[Live9](https://gitlab.com/live9)
