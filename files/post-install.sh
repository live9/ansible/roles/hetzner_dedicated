#!/bin/bash
set -euo pipefail

touch /lock-post-install
echo "Post install script lock installimage until Ansible pickup the ssh fingerprint"
while [ -f /lock_post-install ] ; do
	sleep 1
done
